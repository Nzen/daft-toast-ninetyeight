
/* Daft toast 98 copyright Nicholas Prado; released under ISC license terms */

package ws.nzen.webIo.message;

/** A message with a type. This can serve as a flag, ex shutdown */
public class TypeMessage implements HasType
{
	public static final String TYPE_IDENTITY = "identity";
	public static final String TYPE_NEW_VALUE = "newVal";
	public static final String TYPE_UNDO = "undo";
	public static final String TYPE_REQ_IDENTITY = "identify";
	protected int version;
	protected String msgType;


	public TypeMessage()
	{
	}


	@Override
	public String getMsgType()
	{
		return msgType;
	}

	@Override
	public int getVersion()
	{
		return version;
	}

	public TypeMessage setVersion( int version )
	{
		this.version = version;
		return this;
	}

	public TypeMessage setMsgType( String msgType )
	{
		this.msgType = msgType;
		return this;
	}
	

}
