
/* Daft toast 98 copyright Nicholas Prado; released under ISC license terms */

package ws.nzen.webIo.message;

/** A message to describe which value to replace with what.
	The message's type may indicate the type of thing to replace */
public class ValueUpdate extends TypeMessage
{
	private String oldValue;
	private String newValue;

	public ValueUpdate()
	{
		super();
		setVersion( 2 );
	}

	public String getOldValue()
	{
		return oldValue;
	}

	public ValueUpdate setOldValue( String oldValue )
	{
		this.oldValue = oldValue;
		return this;
	}

	public String getNewValue()
	{
		return newValue;
	}

	public ValueUpdate setNewValue( String newValue )
	{
		this.newValue = newValue;
		return this;
	}

}
