
/* Daft toast 98 copyright Nicholas Prado; released under ISC license terms */

package ws.nzen.webIo.message;

/** A message with a (string) value */
public class ValueMessage extends TypeMessage
{
	private String value;

	public ValueMessage()
	{
		super();
		setVersion( 1 );
	}

	public String getValue()
	{
		return value;
	}

	public ValueMessage setValue( String value )
	{
		this.value = value;
		return this;
	}

}





























