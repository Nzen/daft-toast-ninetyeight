
/* Daft toast 98 copyright Nicholas Prado; released under ISC license terms */

package ws.nzen.webIo.message;

public enum DaftToastType
{
	NEW_NAME, MODEL, REJECTION,
	RESERVE, HASSLE, UNHANDLED;


	public DaftToastType fromString( String fromBrowser )
	{
		if ( NEW_NAME.name().equals( fromBrowser ) )
		{
			return NEW_NAME;
		}
		else if ( MODEL.name().equals( fromBrowser ) )
		{
			return MODEL;
		}
		else if ( REJECTION.name().equals( fromBrowser ) )
		{
			return REJECTION;
		}
		else if ( RESERVE.name().equals( fromBrowser ) )
		{
			return RESERVE;
		}
		else if ( HASSLE.name().equals( fromBrowser ) )
		{
			return HASSLE;
		}
		else
		{
			return UNHANDLED;
		}
	}

}
