
/* Daft toast 98 copyright Nicholas Prado; released under ISC license terms */

package ws.nzen.webIo.message;

/** A message that sends the entire model in an array */
public class ModelBundle extends TypeMessage
{
	private Object[] theModel;


	public ModelBundle()
	{
		super();
		setVersion( 1 );
	}


	public ModelBundle setTheModel( Object[] theModel )
	{
		this.theModel = theModel;
		return this;
	}

}
