
/* Daft toast 98 copyright Nicholas Prado; released under ISC license terms */

package ws.nzen.webIo.message;

public interface HasType
{
	public String getMsgType();


	public int getVersion();
}
