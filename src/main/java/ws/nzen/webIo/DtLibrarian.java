
/* copyright Nicholas Prado; released under ISC license terms */

package ws.nzen.webIo;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.time.LocalTime;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.TreeMap;

import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;

import com.google.gson.Gson;

import ws.nzen.webIo.message.DaftToastType;
import ws.nzen.webIo.message.ModelBundle;
import ws.nzen.webIo.message.TypeMessage;
import ws.nzen.webIo.message.ValueMessage;
import ws.nzen.webIo.message.ValueUpdate;

/** websocket server that handles ip resource checkouts */
public class DtLibrarian extends WebSocketServer
{
	private Gson jsParser = new Gson();
	private String previousModel = "";
	private String model = "initial value";
	private int dtServerVersion = 1;
	private Map<String, DtResource> connLibrary = new TreeMap<>();
	/** ip-hash : Dclient */
	private Map<Integer, DtClient> lendees = new HashMap<>();
	/** DtClient.ip-hash : websocket-hash */
	private Map<Integer, Integer> sessionMetadata = new HashMap<>();
	/** to avoid searching lendees for the same info */
	private int activeUnnammedGuests = -1;
	static final String RESOURCE_FILE = "clients.properties";
	static final String USER_FILE = "users.properties";
	private static final String SINK_CONNECTION = "sink";
	private static final String CLIENT_PREFIX = "ne64:";


	public static void main( String[] args ) throws UnknownHostException
	{
		DtLibrarian conch = new DtLibrarian();
		conch.initSessionFromPrevious();
		conch.start();
		System.out.println( "Server ready for clients. Restart them if they're open." );
	}


	public DtLibrarian() throws UnknownHostException
	{
		super( new InetSocketAddress( 9998 ) );
	}


	@SuppressWarnings("unchecked")
	/** check for resource and client */
	private void initSessionFromPrevious()
	{
		String resourceFileName = RESOURCE_FILE;
		Properties clientConnections = getConfigMapOf( resourceFileName );
		Enumeration<String> loop = (Enumeration<String>)clientConnections
				.propertyNames();
		while ( loop.hasMoreElements() )
		{
			String temp = loop.nextElement();
			DtResource savedConnection = new DtResource();
			savedConnection.name = temp;
			savedConnection.ipAddress = clientConnections.getProperty( temp );
			connLibrary.put( savedConnection.name, savedConnection );
		}
		String leaseeFileName = USER_FILE;
		Properties leaseeConnections = getConfigMapOf( leaseeFileName );
		loop = (Enumeration<String>)leaseeConnections
				.propertyNames();
		while ( loop.hasMoreElements() )
		{
			String temp = loop.nextElement();
			DtClient savedConnection = new DtClient();
			savedConnection.name = temp;
			savedConnection.ipAddress = leaseeConnections.getProperty( temp );
			lendees.put( savedConnection.ipAddress.hashCode(), savedConnection );
		}
	}


	private Properties getConfigMapOf( String resourceFileName )
	{
		Properties resourceMap = new Properties();
		try ( FileReader ioIn = new FileReader( resourceFileName ) )
		{
			resourceMap.load( ioIn );
			if ( resourceMap.size() < 1 )
			{
				System.err.println( "No resources in "+ resourceFileName );
			}
		}
		catch ( FileNotFoundException e )
		{
			System.err.println( "Couldn't find "+ resourceFileName );
			e.printStackTrace();
		}
		catch ( IOException e )
		{
			System.err.println( "Couldn't read "+ resourceFileName );
			e.printStackTrace();
		}
		return resourceMap;
	}


	private void persistUserConfig()
	{
		try ( FileWriter persister = new FileWriter( USER_FILE ) )
		{
			Properties knowsFormat = new Properties();
			for ( Integer userId : lendees.keySet() )
			{
				DtClient currUser = lendees.get( userId );
				knowsFormat.put( currUser.name, currUser.ipAddress );
			}
			knowsFormat.store( persister, "" );
		}
		catch ( IOException ie )
		{
			System.err.println( "Couldn't write "+ USER_FILE
					+" because "+ ie );
		}
	}


	@Override
	public void onOpen( WebSocket conn, ClientHandshake handshake )
	{
		System.out.println( "began with "+ conn.getRemoteSocketAddress()
				+" id-"+ conn.hashCode() );
		conn.send( registerUser( conn.hashCode(), conn
				.getRemoteSocketAddress().getAddress().getHostAddress() ) );
		conn.send( provideEntireModel() );
	}


	@Override
	public void onMessage( WebSocket conn, String message )
	{
		System.out.println( "received "+ message +" "+ conn.hashCode() );
		String reply = replyToMessage( conn.hashCode(),
				conn.getRemoteSocketAddress().getAddress()
				.getHostAddress(), message );
		if ( ! ( reply == null || reply.isEmpty() ) )
		{
			System.out.println( " replied "+ reply +" "+ conn.hashCode() );
			conn.send( reply );
		}
	}


	@Override
	public void onClose( WebSocket conn, int code, String reason, boolean remote )
	{
		// deregister without forgetting user ip
		System.out.println( "finished with "+ conn
				.getRemoteSocketAddress().getAddress().getHostAddress() );
		int ipAsId = conn.getRemoteSocketAddress().hashCode();
		if ( sessionMetadata.containsKey( ipAsId ) )
		{
			sessionMetadata.put( ipAsId, -1 );
		}
		// else, who cares
	}


	@Override
	public void onError( WebSocket conn, Exception ex )
	{
		String connId = ( conn != null  )
				? conn.getRemoteSocketAddress().getAddress().toString() : "null socket";
		System.err.println( "failed with "+ connId +"\n"+ ex );
	}


	/** get saved name or a new guest name; for when they start a session */
	private String registerUser( int sessionId, String ipAddress )
	{
		int ipAsId = ipAddress.hashCode();
		String nameToAdopt;
		if ( ! lendees.containsKey( ipAsId ) )
		{
			DtClient newUser = new DtClient();
			newUser.name = getNameForNewUser();
			newUser.ipAddress = ipAddress;
			nameToAdopt = newUser.name;
			lendees.put( ipAsId, newUser );
			sessionMetadata.put( ipAsId, sessionId );
			persistUserConfig(); // preferably after sending, but that's more io
		}
		else
		{
			nameToAdopt = lendees.get( ipAsId ).name;
			// potentially update session
			if ( sessionMetadata.containsKey( ipAsId ) )
			{
				if ( sessionId != sessionMetadata.get( ipAsId ) )
				{
					sessionMetadata.put( ipAsId, sessionId );
				}
			}
			else
			{
				// new record
				sessionMetadata.put( ipAsId, sessionId );
			}
		}
		ValueUpdate tellsUserItsName = new ValueUpdate();
		String yourPresentNameFlag = "ux9-yourPresentName";
		tellsUserItsName.setOldValue( yourPresentNameFlag );
		tellsUserItsName.setNewValue( nameToAdopt );
		tellsUserItsName.setMsgType( DaftToastType.NEW_NAME.name() );
		tellsUserItsName.setVersion( dtServerVersion );
		return jsParser.toJson( tellsUserItsName );
	}


	private String getNameForNewUser()
	{
		activeUnnammedGuests++;
		return "Guest"+ activeUnnammedGuests;
	}


	private String userRequestsNameChange( 
			int sessionId, String ipAddress,
			String priorName, String requestedName )
	{
		Integer ipAsId = ipAddress.hashCode();
		if ( sessionMetadata.containsKey( ipAsId ) )
		{
			if ( sessionId != sessionMetadata.get( ipAsId ) )
			{
				// update connection info
				sessionMetadata.put( ipAsId, sessionId );
			}
			if ( lendees.containsKey( ipAsId ) )
			{
				DtClient requestant = lendees.get( ipAsId );
				if ( requestant.name.equals( priorName ) )
				{
					requestant.name = requestedName;
					ValueUpdate namingSuccess = new ValueUpdate();
					namingSuccess.setOldValue( priorName )
							.setNewValue( requestedName )
							.setMsgType( DaftToastType.NEW_NAME.name() );
					String successMsg = jsParser.toJson( namingSuccess );
					propagateExceptTo( sessionId, successMsg );
					persistUserConfig(); // preferably after sending, but that's more io
					return successMsg;
				}
				else
				{
					String rejectionReason = "sent a stale name to change from";
					// or should I just propagate to everyone?
					ValueMessage changeRejected = new ValueMessage();
					changeRejected.setValue( rejectionReason )
							.setMsgType( DaftToastType.REJECTION.name() );
					return jsParser.toJson( changeRejected );
				}
			}
			else
			{
				// else version of register that accepts a suggestion
				return registerUser( sessionId, ipAddress );
			}
		}
		else
		{
			// else version of register that accepts a suggestion
			return registerUser( sessionId, ipAddress );
		}
	}


	private void propagateExceptTo( int initiatorSessionId,
			String packagedMessage )
	{
		for ( WebSocket currConnection : connections() )
		{
			if ( currConnection.hashCode() != initiatorSessionId )
			{
				currConnection.send( packagedMessage );
			}
		}
	}


	private String replyToMessage( int sessionId,
			String ipAddress, String json )
	{
		TypeMessage firstPass = jsParser.fromJson( json, TypeMessage.class );
		if ( ! sessionMetadata.containsKey( ipAddress.hashCode() ) )
		{
			// if I don't know you, I'm ignoring you
			return registerUser( sessionId, ipAddress );
		}
		switch ( DaftToastType.UNHANDLED.fromString( firstPass.getMsgType() ) )
		{
			case NEW_NAME :
			{
				ValueUpdate hasNewName = jsParser.fromJson(
						json, ValueUpdate.class );
				return userRequestsNameChange(  sessionId, ipAddress,
						hasNewName.getOldValue(), hasNewName.getNewValue() );
			}
			case RESERVE :
			{
				ValueMessage hasConnName = jsParser.fromJson(
						json, ValueMessage.class );
				return reservationRequest( sessionId,
						ipAddress, hasConnName.getValue() );
			}
			default :
			{
				return null ; // TODO ; says nothing back
			}
		}
	}


	@Deprecated /* change from the multi-listener-type version */
	private String replyToMessage( int sessionId, String json )
	{
		TypeMessage firstPass = jsParser.fromJson( json, TypeMessage.class );
		if ( ! ( firstPass.getMsgType().equals( TypeMessage.TYPE_IDENTITY )
				|| sessionMetadata.containsKey( sessionId ) ) )
		{
			// if I don't know you, I'm ignoring you
			return requestIdentity();
		}
		switch ( firstPass.getMsgType() )
		{
			case "NEW_NAME" : // FIX DaftToastTypes.NEW_NAME.name() : // not a constant expression %U
			{
				ValueUpdate hasNewVal = jsParser.fromJson( json, ValueUpdate.class );
				return acknowledgeIdentity( sessionId, hasNewVal.getNewValue() );
			}
			case TypeMessage.TYPE_IDENTITY :
			{
				ValueUpdate hasNewVal = jsParser.fromJson( json, ValueUpdate.class );
				return acknowledgeIdentity( sessionId, hasNewVal.getNewValue() );
			}
			case TypeMessage.TYPE_NEW_VALUE :
			{
				ValueUpdate hasNewVal = jsParser.fromJson( json, ValueUpdate.class );
				return propagateNewModel( sessionId, hasNewVal.getNewValue() );
			}
			case TypeMessage.TYPE_UNDO :
			{
				return undoLastChange( sessionId );
			}
			default :
			{
				return "";
			}
		}
	}


	@Deprecated /* change from the multi-listener-type version */
	/** unknown connection made a request; ignore and demand identity */
	private String requestIdentity()
	{
		TypeMessage sayYourName = new TypeMessage();
		sayYourName.setMsgType( "identify" );
		sayYourName.setVersion( 1 );
		return jsParser.toJson( sayYourName, TypeMessage.class );
	}


	@Deprecated /* change from the multi-listener-type version */
	private String acknowledgeIdentity( int sessionId, String reportedType )
	{
		if ( reportedType.startsWith( CLIENT_PREFIX ) )
		{
			/*sessionMetadata.put( sessionId, reportedType
					.substring( CLIENT_PREFIX.length() ) );*/
		}
		return ""; // not really acknowledging }:)
	}


	@Deprecated /* change from the multi-listener-type version */
	private String undoLastChange( int initiatorId )
	{
		return propagateNewModel( initiatorId, previousModel );
	}


	@Deprecated /* change from the multi-listener-type version */
	private String propagateNewModel( int initiatorId, String newVal )
	{
		// handle the update
		previousModel = model;
		model = newVal;
		ValueUpdate hasNewVal = new ValueUpdate();
		hasNewVal.setNewValue( model ).setMsgType( TypeMessage.TYPE_NEW_VALUE );
		String replyForSink = jsParser.toJson( hasNewVal );
		hasNewVal.setNewValue( hasNewVal.getNewValue().toUpperCase() );
		String replyForListener = jsParser.toJson( hasNewVal );
		// warn everyone except initiator
		int currId;
		for ( WebSocket currConnection : connections() )
		{
			currId = currConnection.hashCode();
			if ( currId != initiatorId )
			{
				if ( sessionMetadata.get( currId ).equals( SINK_CONNECTION ) )
				{
					currConnection.send( replyForSink );
				}
				else
				{
					currConnection.send( replyForListener );
				}
			}
		}
		if ( sessionMetadata.get( initiatorId ).equals( SINK_CONNECTION ) )
		{
			return replyForSink;
		}
		else
		{
			return replyForListener;
		}
	}


	/** json array of the DtResources, with updated client fields */
	private String provideEntireModel()
	{
		DtResource[] bundled = new DtResource[ connLibrary.size() ];
		int ind = 0;
		for ( String clientId : connLibrary.keySet() )
		{
			DtResource client = connLibrary.get( clientId );
			if ( client.leasee == DtResource.ID_WHEN_FREE )
			{
				client.user = DtResource.USER_WHEN_FREE;
			}
			else
			{
				client.user = lendees.get( client.leasee ).name;
			}
			client.refreshDurationFields();
			bundled[ ind ] = client;
			ind++;
		}
		ModelBundle everything = new ModelBundle();
		everything.setMsgType( DaftToastType.MODEL.name() );
		everything.setVersion( dtServerVersion );
		everything.setTheModel( bundled );
		return jsParser.toJson( everything );
	}


	private String reservationRequest( Integer sessionId,
			String ipAddress, String resourceName )
	{
		Integer ipAsId = ipAddress.hashCode();
		if ( sessionMetadata.containsKey( ipAsId ) )
		{
			// check if conn is out of date ?
			if ( lendees.containsKey( ipAsId ) )
			{
				DtResource updatedClient = null;
				for ( String clientId : connLibrary.keySet() )
				{
					DtResource currLease = connLibrary.get( clientId );
					if ( currLease.name.equals( resourceName ) )
					{
						if ( currLease.leasee == ipAsId )
						{
							// IMPROVE have the DtR do all this
							// release
							currLease.leasee = DtResource.ID_WHEN_FREE;
							currLease.user = DtResource.USER_WHEN_FREE;
							currLease.leaseBegan = null;
							currLease.refreshDurationFields();
							updatedClient = currLease;
							break; // should only be one
						}
						else if ( currLease.leasee == DtResource.ID_WHEN_FREE )
						{
							// allocate
							currLease.leasee = ipAsId;
							if ( lendees.containsKey( ipAsId ) )
							{
								currLease.user = lendees.get( ipAsId ).name;
							}
							currLease.leaseBegan = LocalTime.now();
							currLease.refreshDurationFields();
							updatedClient = currLease;
							break;
						}
						else
						{
							// asking to release someone else's: deny
							ValueMessage reject = new ValueMessage();
							reject.setValue( Boolean.toString(
									currLease.leasee == ipAsId ) );
							// maybe a field message?
							return null; // silently rejected, for now
						}
					}
				}
				if ( updatedClient != null )
				{
					ModelBundle updatedRow = new ModelBundle();
					updatedRow.setTheModel( new DtResource[]{ updatedClient } )
							.setMsgType( DaftToastType.RESERVE.name() );
					String modelDiff = jsParser.toJson( updatedRow );
					propagateExceptTo( sessionId, modelDiff );
					return modelDiff;
				}
			}
		}
		return null; // request denied
	}


}








































