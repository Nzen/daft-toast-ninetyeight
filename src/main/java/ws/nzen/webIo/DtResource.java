
/* Daft toast 98 copyright Nicholas Prado; released under ISC license terms */

package ws.nzen.webIo;

import java.io.Serializable;
import java.time.Duration;
import java.time.LocalTime;

/** a thing to checkout */
public class DtResource implements Serializable
{
	private static final long serialVersionUID = 1L;
	public static int ID_WHEN_FREE = -1;
	public static String USER_WHEN_FREE = "_____";
	public String name;
	// address of the resource
	public String ipAddress;
	// ip-hash of leasee
	public int leasee = ID_WHEN_FREE;
	public LocalTime leaseBegan = null;
	/** denormalizations for the client */
	public String user;
	public long hoursUsed = 0;
	public long remainderMinutesUsed = 0;


	public void refreshDurationFields()
	{
		if ( leaseBegan == null )
		{
			hoursUsed = 0L;
			remainderMinutesUsed = 0L;
			return;
		}
		Duration howLong = Duration
				.between( leaseBegan, LocalTime.now() );
		hoursUsed = howLong.toHours();
		long totalMinutes = howLong.toMinutes();
		remainderMinutesUsed = totalMinutes
				- ( hoursUsed * 60L );
	}


	@Override
	public String toString()
	{
		return "DR: n "+ name +" ip "+ ipAddress
				+" for "+ leasee;
	}
}
