
/* Daft toast 98 copyright Nicholas Prado; released under ISC license terms */

package ws.nzen.webIo;

public class DtClient
{
	private static final long serialVersionUID = 1L;
	public String name;
	public String ipAddress;


	@Override
	public String toString()
	{
		return "DC: n "+ name +" ip "+ ipAddress;
	}

}
