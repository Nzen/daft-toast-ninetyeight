
/* Daft toast 98 copyright Nicholas Prado; released under ISC license terms */

// prefixes for ids of the various types of elements we'll use
const nameInputElem = 'tfName';
const checkElem = 'check';
const connElem = 'conn';
const userElem = 'name';
const timeElem = 'leaseTime';
const hassleElem = 'hassle';
const rowElem = 'tr';
const targetElem = 'where';
const connTableElem = 'connectionList';
const styleSelf = 'showsSelf';
const styleOther = 'showsOther';
const styleNone = 'showsNone';
const unusedConnUser = '_____';
const replaceMyCurrentNameFlag = 'ux9-yourPresentName';
var myUserName = '';
var currentMaxId = -1;


/** user changed name; notify the server */
function changedName()
{
	// I'll let showNewName() change myUserName
	channel.send( JSON.stringify( { 'msgType' : 'NEW_NAME',
		'oldValue' : myUserName,
		'newValue' : document.getElementById( nameInputElem ).value,
		'version' : messageVersionNewVal } ) );
}


/** server provided new name for someone; including me */
function showNewName( oldName, newName )
{
	if ( oldName == myUserName
			|| oldName == replaceMyCurrentNameFlag )
	{
		myUserName = newName;
		document.getElementById( nameInputElem ).value = myUserName;
	}
	// update the table
	var nameSpan;
	for ( var ind = 0; ind < currentMaxId; ind++ )
	{
		nameSpan = document.getElementById( userElem + ind );
		if ( nameSpan.innerHTML == oldName )
		{
			nameSpan.innerHTML = newName;
		}
	}
}


/* TODO ask server for an interrupt to that person */
function hassle( rowId )
{
	termin.log( '4TESTS hassled '+ rowId );
}
// and then receive a hassle request ; and then receive a hassle response


/** ask server for this connection.
	caller (and server) handles whether this is a free connection */
function reserveConnection( rowId )
{
	var connectionName = document
			.getElementById( targetElem + rowId );
	/*var terMsg = ( document.getElementById(
		checkElem + rowId ).checked ) ? 'reserve' : 'deallocate';
	termin.log( terMsg' '+ connectionName.innerText );*/
	channel.send( JSON.stringify( { 'msgType' : 'RESERVE',
		'value' : connectionName.innerText,
		'version' : messageVersionNewVal } ) );
}


/** update row with new DtResource info (user or lease time) */
function updateConnection( dtResourceAsArray )
{
	/*
	get it out
	find the row
	update values of that row
	replace row
	
	I'm faced with the choice of holding a view model (mapping)
	of connId to resource name (which is what I care about)
	OR iterating through the dom. latter for now
	
	and then, mutate values or cache-and-replace the whole table?
	*/
	for ( var dtInd = 0; dtInd < dtResourceAsArray.length; dtInd++ )
	{
		var dtResource = dtResourceAsArray[ dtInd ]; // there should only be one
		var tRowArr = document.getElementById( connTableElem ).children;
		const tdIndOfClientName = 1;
		// ind at 1 so it skips the header row
		for ( var trInd = 1; trInd < tRowArr.length; trInd++ )
		{
			var tdArr = tRowArr[ trInd ].children;
			/*termin.log( 'dtv.uc row '+ trInd +' for '
					+ tdArr[ tdIndOfClientName ].innerText ); */ // 4TESTS
			if ( tdArr[ tdIndOfClientName ].innerText == dtResource.name )
			{
				tRowArr[ trInd ].className = cssOfUser( dtResource.user );
				// ugly direct mutation
				tRowArr[ trInd ] = rowReflects( tdArr, dtResource );
				// tRowArr[ trInd ] = connectionRowFrom( dtResource ); // didn't update it
				break;
			}
		}
	}
}


// IMPROVE so smell, much hardcoded understanding, chaaannge
function rowReflects( tdArr, dtResource )
{
	// checkbox
	tdArr[ 0 ].disabled = ( dtResource.user != unusedConnUser
			&& dtResource.user != myUserName );
	tdArr[ 0 ].checked = ( dtResource.user != unusedConnUser );
	// connection ; nothing
	tdArr[ 2 ].innerHTML = dtResource.user;
	// lease duration
	tdArr[ 3 ].innerHTML = leaseDuration( dtResource.hoursUsed,
		dtResource.remainderMinutesUsed );
	// hassle
	/* tdArr[ 4 ].hidden = ( dtResource.user == myUserName
		|| dtResource.user != unusedConnUser); */
}


/** dump and recreate the unsorted list with new model */
function addWholeModel( arrayOfDtResource )
{
	currentMaxId = -1;
	document.getElementById( connTableElem ).remove();
	var localRoot = document.createElement( 'table' );
	localRoot.id = connTableElem;
	var headerRow = document.createElement( 'tr' );
	headerRow.innerHTML = '<th>Reserved</th><th>Client</th><th>By whom</th>'
		+'<th>Duration</th><th>;<!--Contact--></th>';
	localRoot.appendChild( headerRow );
	for ( var ind = 0; ind < arrayOfDtResource.length; ind++ )
	{
		localRoot.appendChild( connectionRowFrom(
				arrayOfDtResource[ ind ], ind ) );
	}
	document.getElementById( 'connectionArea' ).appendChild( localRoot );
}


/** row element with supplied id */
function connectionRowFrom( dtResource )
{
	return connectionRowFrom( dtResource, idForNewConnection() );
}


/** returns a row element with info from the dtResource */
function connectionRowFrom( dtResource, connId )
{
	var formNode = document.createElement( 'tr' );
	formNode.className = cssOfUser( dtResource.user );
	// <td><input type = "checkbox" id = "bool14" checked/></td>
	var checkNode = document.createElement( 'input' );
	checkNode.setAttribute( 'type', 'checkbox' );
	checkNode.id = checkElem + connId;
	checkNode.onclick = function()
	{
		reserveConnection( connId );
	};
	checkNode.disabled = ( dtResource.user != unusedConnUser
			&& dtResource.user != myUserName );
	checkNode.checked = ( dtResource.user != unusedConnUser );
	var tdCheck = document.createElement( 'td' );
	tdCheck.appendChild( checkNode );
	formNode.appendChild( tdCheck );
	// <td id = 'conn13'>zardoz</td>
	var tdTarget = document.createElement( 'td' );
	tdTarget.id = targetElem + connId;
	tdTarget.innerHTML = dtResource.name;
	formNode.appendChild( tdTarget );
	// <td id = "tf13" class = "showsOther">Gerard</td>
	var tdUser = document.createElement( 'td' );
	tdUser.id = userElem + connId;
	tdUser.innerHTML = dtResource.user;
	formNode.appendChild( tdUser );
	// <td id = "leaseTime14">4h 3m</td>
	var tdLease = document.createElement( 'td' );
	tdLease.id = timeElem + connId;
	tdLease.innerHTML = leaseDuration( dtResource.hoursUsed,
		dtResource.remainderMinutesUsed );
	formNode.appendChild( tdLease );
	// <td><button type = "button" onclick = "hassle()" id = "hassle13">hassle</button></td>
	/*
		//IMPROVE activate when I can actually support it
	var hassleBtn = document.createElement( 'button' );
	hassleBtn.setAttribute( 'type', 'button' );
	hassleBtn.id = hassleElem + connId;
	hassleBtn.hidden = ( dtResource.user == myUserName );
	hassleBtn.onclick = function()
	{
		hassle( connId );
	};
	btnText = document.createTextNode( 'hassle' );
	hassleBtn.appendChild( btnText );
	var tdHassle = document.createElement( 'td' );
	tdHassle.appendChild( hassleBtn );
	formNode.appendChild( tdHassle );
	*/
	return formNode
}


function cssOfUser( user )
{
	if ( user == myUserName )
	{
		return styleSelf;
	}
	else if ( user != unusedConnUser )
	{
		return styleOther;
	}
	else
	{
		return styleNone;
	}
}


function leaseDuration( hours, minutes )
{
	var asText = '';
	if ( hours > 0 )
	{
		asText = ' '+ hours +'h';
	}
	if ( minutes > 0 )
	{
		 asText += ' '+ minutes +'m';
	}
	return asText;
}


// this is traditionally an occasion for a closure around that var
function idForNewConnection()
{
	currentMaxId++;
	return currentMaxId;
}






































