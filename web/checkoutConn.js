
/* Daft toast 98 copyright Nicholas Prado; released under ISC license terms */

var termin = function() {};
termin.log = function( message )
{
	try
	{ console.log( message ); }
	catch ( exception )
	{ return; } // IE reputedly has no console.
}


var channel;
//@ unchecked ; old version // change to m_type_Accept- / SentVersion
const messageVersionIdentity = 1;
const messageVersionNewVal = 1;


var supported = 'WebSocket' in window;
if ( ! supported )
{
	alert( 'This browser doesn\'t support necessary tech, sorry' );
}
else
{
	channel = new WebSocket( 'ws://localhost:9998' );
	channel.onopen = function coo()
	{
		termin.log( 'connected' );
	};
	channel.onclose = function coc()
	{
		termin.log( 'session over' );
		alert( 'The server disconnected our session' );
	};
	channel.onmessage = function com( msg )
	{
		applyResponse( msg.data );
	};
}


//show the new value
function applyResponse( stillJson )
{
	var response = JSON.parse( stillJson );
	// IMPROVE use actual version numbers
	if ( response.version < messageVersionIdentity )
	{
		termin.log( 'server sent deprecated message version '+ response.version );
	}
	else if ( response.msgType == 'NEW_NAME' )
	{
		showNewName( response.oldValue, response.newValue );
	}
	else if ( response.msgType == 'MODEL' )
	{
		addWholeModel( response.theModel );
	}
	else if ( response.msgType == 'RESERVE' )
	{
		updateConnection( response.theModel );
	}
	else if ( response.msgType == 'REJECTION' )
	{
		termin.log( 'server rejected because : '+ response.value );
	}
	else
	{
		termin.log( response.msgType +' is not a response I handle' );
		// perhaps other failure states
	}
}























